﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace QRdemo
{
    class Program
    {
        static Socket serverSocket;

        static void Main(string[] args)
        {
            IPAddress ip = IPAddress.Any;
            serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(new IPEndPoint(ip, 80));
            serverSocket.Listen(10);
            Console.WriteLine("listen {0} success", serverSocket.LocalEndPoint.ToString());
            Thread myThread = new Thread(ListenClientConnect);
            myThread.Start();
            Console.ReadLine();  
        }

        private static void ListenClientConnect()
        {
            while (true)
            {
                Socket clientSocket = serverSocket.Accept();

                Thread receiveThread = new Thread(ReceiveMessage);
                receiveThread.Start(clientSocket);
            }
        }

        private static void ReceiveMessage(object clientSocket)
        {
            Socket myClientSocket = (Socket)clientSocket;
            string str;
            byte[] result = new byte[1524];
            List<string> reply_str;

            int receiveNumber = myClientSocket.Receive(result);
            Console.WriteLine("recive client {0} message {1}", myClientSocket.RemoteEndPoint.ToString(), Encoding.ASCII.GetString(result, 0, receiveNumber));

            str = System.Text.Encoding.Default.GetString(result);
            switch_string(str, out reply_str);
            str = make_res_string(reply_str);

            myClientSocket.Send(Encoding.ASCII.GetBytes(str));

            myClientSocket.Shutdown(SocketShutdown.Both);
            myClientSocket.Close();
        }

        private static string make_res_string(List<string> val_list)
        {
            string reply = "";
            string s1,s3;

            s1 = val_list[1];
            s3 = val_list[3];

            reply += "HTTP/1.1 200 OK\r\n";
            reply += "Date: Sat, 26 Aug 2018 00:55:09 GMT\r\n";
            reply += "Content-Type: text/html\r\n";
            reply += "Connection: close\r\n";
            reply += "Set-Cookie: tgw_17_route=9ae25c10de3bd25fa50d1bc7dd27ba45; Expires=Sat, 26 Aug 2018 00:55:09 GMT; Path=/\r\n";
            reply += "Server: nginx\r\n";
            reply += "Vary: Accept-Encoding\r\n";
            reply += string.Format("\r\n\r\n{{\"data\":[{{\"cardid\":\"{0}\",\"cjihao\":{1},\"mjihao\":{2},\"status\":{3},\"time\":\"{4}\",\"output\":{5}}}],\"code\":{6},\"message\":\"\"}}\r\n"
                    ,"89ABCDEF"/* val_list[0] */
                    ,0/* val_list[2] */
                    ,s1
                    ,0/* status 1:valid(buzzer 2 times) 0:invalid(buzzer 1 times) */
                    ,s3
                    ,1/* output= 0:Access ，1:WG26, 2:WG34   */
                    ,0);/* code=0 */

            return reply;
        }

        /* QRCode send GET /qa/mcardsea.php?cardid=445D2C&mjihao=1&cjihao=HW256097&status=11&time=1540402036 HTTP/1.0 */
        /* Server reply {"data":[{"cardid":"123456","cjihao":0,"mjihao":1,"status":1,"time":"0928162352","output":2}],"code":0,"message":""} */
        //                  {"data":[{"cardid":"89ABCDEF","cjihao":0,"mjihao":1,"status":0,"time":"1540387800","output":1}],"code":0,"message":""}
        private static void switch_string(string str, out List<string> val_list)
        {
            int position;
            string [] str_list, sub_str_list;
            string title,val;
            DateTime dt;
            long t;

            val_list = new List<string>();

            position = str.IndexOf("/qa/mcardsea", 0);
            if( -1 == position )
            {
                return;
            }
            position = str.IndexOf("HTTP", 0);
            if( -1 == position )
            {
                return;
            }

            position = str.IndexOf('?', 0);
            //str = str.mid(position + 1);
            str = str.Substring(position + 1);

            position = str.IndexOf("HTTP", 0);
            //str = str.left(position - 1);
            str = str.Substring(0, position - 1);

            str = str.Replace("\r", string.Empty);
            str = str.Replace("\n", string.Empty);
            //str.remove('\r');
            //str.remove('\n');

            str_list = str.Split('&');

            title = str_list[0];
            sub_str_list = title.Split('=');
            title = sub_str_list[0];
            val = sub_str_list[1];
            val_list.Add(val);/* cardid */
            Console.WriteLine("QRcode value:{0}", val);

            title = str_list[1];
            sub_str_list = title.Split('=');
            title = sub_str_list[0];
            val = sub_str_list[1];
            val_list.Add(val);/* mjihao */
            Console.WriteLine("machine id:{0}", val);

            title = str_list[2];
            sub_str_list = title.Split('=');
            title = sub_str_list[0];
            val = sub_str_list[1];
            val_list.Add(val);/* cjihao */
            Console.WriteLine("machine sn:{0}", val);

            title = str_list[3];
            sub_str_list = title.Split('=');
            title = sub_str_list[0];
            val = sub_str_list[1];
            Console.WriteLine("status:{0}:{1}", (1 == val.Length) ? "Fail" : "Success", ('0' == val[0]) ? "Out" : "In");/* status */

            title = str_list[4];
            sub_str_list = title.Split('=');
            title = sub_str_list[0];
            val = sub_str_list[1];/* time */
            val_list.Add(val);

            t = int.Parse(val);
            dt = DateTime.FromFileTimeUtc(t);
            Console.WriteLine("time:{0}", dt);
        }
    }
}
