TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = qrcode_demo
DESTDIR = ../build-$${TARGET}

UI_DIR = $${DESTDIR}
MOC_DIR = $${DESTDIR}
RCC_DIR = $${DESTDIR}
OBJECTS_DIR = $${DESTDIR}

INCLUDEPATH += .

CONFIG += debug

#win32{
#LIBS += ws2_32.lib
#}

SOURCES += \
    demo.cpp




