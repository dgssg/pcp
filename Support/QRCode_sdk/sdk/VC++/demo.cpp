


#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>

#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <algorithm>

#pragma comment(lib,"WS2_32.lib")

using namespace std;

void make_res_string(string *str, std::vector<string> *val_list);
void switch_string(string str, std::vector<string> *val_list);

int main(int argc, char* argv[])
{
    WORD sockVersion = MAKEWORD(2,2);
    WSADATA wsaData;
    string str;
    std::vector<string> reply_str;

    if(WSAStartup(sockVersion, &wsaData)!=0)
    {
        return 0;
    }

    SOCKET slisten = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(slisten == INVALID_SOCKET)
    {
        printf("socket error !");
        return 0;
    }

    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(80);
    sin.sin_addr.S_un.S_addr = INADDR_ANY;
    if(bind(slisten, (LPSOCKADDR)&sin, sizeof(sin)) == SOCKET_ERROR)
    {
        printf("bind error !");
    }

    if(listen(slisten, 1) == SOCKET_ERROR)
    {
        printf("listen error !");
        return 0;
    }

    SOCKET sClient;
    sockaddr_in remoteAddr;
    int nAddrlen = sizeof(remoteAddr);
    char revData[1500];
    while (true)
    {
        printf("wait connect...\n");
        sClient = accept(slisten, (SOCKADDR *)&remoteAddr, &nAddrlen);
        if(sClient == INVALID_SOCKET)
        {
            printf("accept error !");
            continue;
        }
        printf("accept qrcode: %s \r\n", inet_ntoa(remoteAddr.sin_addr));

        //接收数据
        memset(revData, 0, sizeof(revData));
        reply_str.clear();
        str.clear();
        int ret = recv(sClient, revData, sizeof(revData), 0);
        if(ret > 0)
        {
            revData[ret] = 0x00;
            //printf(revData);
            str = revData;
            switch_string(str, &reply_str);
            make_res_string(&str, &reply_str);
        }

        //发送数据
        send(sClient, str.c_str(), str.length(), 0);
        closesocket(sClient);
    }

    closesocket(slisten);
    WSACleanup();

    return 0;
}


void supersplit(const std::string& s, std::vector<std::string>& v, const std::string& c)
{
    std::string::size_type pos1, pos2;
    size_t len = s.length();
    pos2 = s.find(c);
    pos1 = 0;
    while(std::string::npos != pos2)
    {
        v.emplace_back(s.substr(pos1, pos2-pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if(pos1 != len)
        v.emplace_back(s.substr(pos1));
}

void make_res_string(string *str, std::vector<string> *val_list)
{
    char reply[1500] = {0};
    string s1,s3;

    s1 = val_list->at(1);
    s3 = val_list->at(3);

    str->clear();
    sprintf(reply, "HTTP/1.1 200 OK\r\n"
                   "Date: Sat, 26 Aug 2018 00:55:09 GMT\r\n"
                   "Content-Type: text/html\r\n"
                   "Connection: close\r\n"
                   "Set-Cookie: tgw_17_route=9ae25c10de3bd25fa50d1bc7dd27ba45; Expires=Sat, 26 Aug 2018 00:55:09 GMT; Path=/\r\n"
                   "Server: nginx\r\n"
                   "Vary: Accept-Encoding\r\n"
                   "\r\n\r\n{\"data\":[{\"cardid\":\"%s\",\"cjihao\":%d,\"mjihao\":%s,\"status\":%d,\"time\":\"%s\",\"output\":%d}],\"code\":%d,\"message\":\"\"}\r\n"
            ,"89ABCDEF"/* val_list[0] */
            ,0/* val_list[2] */
            ,s1.c_str()
            ,0/* status 1:valid(buzzer 2 times) 0:invalid(buzzer 1 times) */
            ,s3.c_str()
            ,1/* output= 0:Access ，1:WG26, 2:WG34   */
            ,0);/* code=0 */

    *str = reply;
}

/* QRCode send GET /qa/mcardsea.php?cardid=445D2C&mjihao=1&cjihao=HW256097&status=11&time=1540402036 HTTP/1.0 */
/* Server reply {"data":[{"cardid":"123456","cjihao":0,"mjihao":1,"status":1,"time":"0928162352","output":2}],"code":0,"message":""} */
//                  {"data":[{"cardid":"89ABCDEF","cjihao":0,"mjihao":1,"status":0,"time":"1540387800","output":1}],"code":0,"message":""}
void switch_string(string str, std::vector<string> *val_list)
{
    int position;
    std::vector<string> str_list, sub_str_list;
    string title,val;
    time_t t = time(NULL);
    string tmp_str;
    struct tm *tm_t;

    val_list->clear();

    position = str.find("/qa/mcardsea", 0);
    if( -1 == position )
    {
        return;
    }
    position = str.find("HTTP", 0);
    if( -1 == position )
    {
        return;
    }

    position = str.find('?', 0);
    //str = str.mid(position + 1);
    str = str.substr(position + 1);

    position = str.find("HTTP", 0);
    //str = str.left(position - 1);
    str = str.substr(0, position - 1);

    //str.remove('\r');
    //str.remove('\n');
    str.erase(remove(str.begin(), str.end(), '\r'), str.end());
    str.erase(remove(str.begin(), str.end(), '\n'), str.end());

    //str_list = str.split('&');
    supersplit(str, str_list, "&");

    title = str_list[0];
    //sub_str_list = title.split('=');
    supersplit(title, sub_str_list, "=");
    title = sub_str_list[0];
    val = sub_str_list[1];
    sub_str_list.clear();
    //val_list->append(val);/* cardid */
    val_list->push_back(val);
//    ui->label_id->setText(val);
    cout << "QRcode value:" << ":" << val << endl;

    title = str_list[1];
    //sub_str_list = title.split('=');
    supersplit(title, sub_str_list, "=");
    title = sub_str_list[0];
    val = sub_str_list[1];
    sub_str_list.clear();
    //val_list->append(val);/* mjihao */
    val_list->push_back(val);
//    ui->label_machine->setText(val);
    cout << "machine id" << ":" << val << endl;

    title = str_list[2];
    //sub_str_list = title.split('=');
    supersplit(title, sub_str_list, "=");
    title = sub_str_list[0];
    val = sub_str_list[1];
    sub_str_list.clear();
    //val_list->append(val);/* cjihao */
    val_list->push_back(val);
//    ui->label_sn->setText(val);
    cout << "machine sn" << ":" << val << endl;

    title = str_list[3];
    //sub_str_list = title.split('=');
    supersplit(title, sub_str_list, "=");
    title = sub_str_list[0];
    val = sub_str_list[1];
    sub_str_list.clear();
    cout << "status:";
    if( 1 == val.length() )/* status */
    {
        //ui->label_status->setText(tr("Fail"));
        cout << "Fail:";
        if( '0' == val[0] )
        {
            //ui->label_in_out->setText(tr("Out"));
            cout << "Out" << endl;
        }else
        {
            //ui->label_in_out->setText(tr("In"));
            cout << "In" << endl;
        }
    }else if( 2 == val.length() )
    {
        //ui->label_status->setText(tr("Success"));
        cout << "Success:";
        if( '0' == val[1] )
        {
            //ui->label_in_out->setText(tr("Out"));
            cout << "Out" << endl;
        }else
        {
            //ui->label_in_out->setText(tr("Out"));
            cout << "Out" << endl;
        }
    }

    title = str_list[4];
    //sub_str_list = title.split('=');
    supersplit(title, sub_str_list, "=");
    title = sub_str_list[0];
    val = sub_str_list[1];/* time */
    sub_str_list.clear();

//    tmp_str = QString("%1").arg(t);
//    val_list->append(tmp_str);
    t = atol(val.c_str());
    val_list->push_back(val);
    tm_t = localtime(&t);
    val = asctime(tm_t);
    cout << "time" << ":" << val << endl;
}

